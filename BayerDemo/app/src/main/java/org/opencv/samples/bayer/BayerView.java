package org.opencv.samples.bayer;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.opencv.android.JavaCameraView;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;

import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class BayerView extends JavaCameraView implements PictureCallback, Camera.AutoFocusCallback {

    private static final String TAG = "Sample::Tutorial3View";
    private String mPictureFileName;
    private Detector mDetector;
    Matrix matrix;
    int focusAreaSize;
    boolean bTakeFoto = false;

    public BayerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        focusAreaSize = 50;//getResources().getDimensionPixelSize(R.dimen.camera_focus_area_size);
        matrix = new Matrix();

    }


    @Override
    public void onAutoFocus(boolean focused, Camera camera) {
        //play default system sound if exists
        if (focused) {
            float focusDistances[] = new float[3];
            camera.getParameters().getFocusDistances(focusDistances);
            Log.e("CameraTag",String.format("%f %f %f",focusDistances[0],focusDistances[1],focusDistances[2]));
            //focusSound.play();
        }else {
            Log.e("CameraTag",String.format("no focus"));
            //focusSound.play();
        }

        if(bTakeFoto){
            bTakeFoto = false;
            mCamera.setPreviewCallback(null);
            mCamera.takePicture(null, null, this) ;
            /*
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    mCamera.setPreviewCallback(null);
                    mCamera.takePicture(null, null, new PictureCallback() {
                        public void onPictureTaken(byte[] data, Camera _camera) {
                            // The camera preview was automatically stopped. Start it again.
                            mCamera.startPreview();
                            //mCamera.setPreviewCallback(this);

                             mDetector.detect(data,"");

                        }
                    });
                }
            }, 500);
            */
        }

    }

    protected void focusOnTouch(MotionEvent event) {
        bTakeFoto = true;
        if (mCamera != null) {
            //cancel previous actions
            mCamera.cancelAutoFocus();

            Rect focusRect = calculateTapArea(event.getX(), event.getY(), 1f);
            Rect meteringRect = calculateTapArea(event.getX(), event.getY(), 1.5f);

            Camera.Parameters parameters = null;
            try {
                parameters = mCamera.getParameters();
            } catch (Exception e) {
                Log.e("CameraTag",e.getMessage());
            }

            // check if parameters are set (handle RuntimeException: getParameters failed (empty parameters))
            if (parameters != null) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                Camera.Area cameraArea = new Camera.Area(focusRect, 1000);
                List areaList = new ArrayList();
                areaList.add(cameraArea);
                parameters.setFocusAreas(areaList);

                if (parameters.getMaxNumMeteringAreas() > 0) {
                    Camera.Area cameraMeteringArea = new Camera.Area(meteringRect, 1000);
                    areaList.clear();
                    areaList.add(cameraMeteringArea);
                    parameters.setMeteringAreas(areaList);
                }

                /*
                if (meteringAreaSupported) {
                    parameters.setMeteringAreas(Collections.newArrayList(new Camera.Area(meteringRect, 1000)));
                }
                */
                try {
                    mCamera.setParameters(parameters);
                    mCamera.autoFocus(this);
                } catch (Exception e) {
                    Log.e("CameraTag",e.getMessage());
                }
            }
        }
    }

    /**
     * Convert touch position x:y to {@link Camera.Area} position -1000:-1000 to 1000:1000.
     * <p>
     * Rotate, scale and translate touch rectangle using matrix configured in
     *
     */
    private Rect calculateTapArea(float x, float y, float coefficient) {

        int areaSize = Float.valueOf(focusAreaSize * coefficient).intValue();


        int left = clamp((int) x - areaSize / 2, 0, mFrameWidth - areaSize);
        int top = clamp((int) y - areaSize / 2, 0, mFrameHeight - areaSize);

        RectF rectF = new RectF(left, top, left + areaSize, top + areaSize);

        matrix.mapRect(rectF);

        return new Rect(Math.round(rectF.left * 2000/mFrameWidth)-1000, Math.round(rectF.top * 2000/mFrameHeight)-1000, Math.round(rectF.right * 2000/mFrameWidth)-1000, Math.round(rectF.bottom * 2000/mFrameHeight)-1000);
    }

    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }


    public List<Size> getPictureResolutionList() {
        return mCamera.getParameters().getSupportedPictureSizes();
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public void setResolution(Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    public Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    public void takePicture(final String fileName) {
        Log.i(TAG, "Taking picture");
        this.mPictureFileName = fileName;
        this.mDetector = null;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null,  this);
    }

    public void setDetector(final Detector detector) {
        Log.i(TAG, "set Detector");
        this.mDetector = detector;
        this.mPictureFileName = null;
    }

    public void detectPicture(final Detector detector) {
        Log.i(TAG, "Detect picture");
        this.mDetector = detector;
        this.mPictureFileName = null;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
    }


    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        if ( this.mPictureFileName != null) {
            // Write the image in a file (in jpeg format)
            try {
                FileOutputStream fos = new FileOutputStream(mPictureFileName);

                fos.write(data);
                fos.close();

            } catch (java.io.IOException e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
            }
        }else{
            mDetector.detect(data,"");

        }

    }
}
