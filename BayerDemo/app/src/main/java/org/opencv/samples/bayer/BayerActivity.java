package org.opencv.samples.bayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

public class BayerActivity extends Activity implements CvCameraViewListener2, OnTouchListener  {
    private static final String TAG = "OCVSample::Activity";

    private BayerView mOpenCvCameraView;
    private List<Size> mPictureResolutionList;
    private MenuItem[] mPictureResolutionMenuItems;
    private SubMenu mPictureResolutionMenu;
    private List<Size> mResolutionList;
    private MenuItem[] mResolutionMenuItems;
    private SubMenu mResolutionMenu;
    private Mat currentFrame = null;

    protected Detector mDetector;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(BayerActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public BayerActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.bayer_surface_view);

        mOpenCvCameraView = (BayerView) findViewById(R.id.bayer_activity_java_surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        //focusAreaSize = getResources().getDimensionPixelSize(R.dimen.camera_focus_area_size);
        // Matrix matrix = new Matrix();
        //FocusSound focusSound = new FocusSound();

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        initDetector();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        currentFrame =  inputFrame.rgba();
        return currentFrame;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        mPictureResolutionMenu = menu.addSubMenu("Resolution");
        mPictureResolutionList = mOpenCvCameraView.getPictureResolutionList();
        mPictureResolutionMenuItems = new MenuItem[mPictureResolutionList.size()];

        ListIterator<Size> resolutionItr = mPictureResolutionList.listIterator();
        int idx = 0;
        while(resolutionItr.hasNext()) {
            Size element = resolutionItr.next();
            mPictureResolutionMenuItems[idx] = mPictureResolutionMenu.add(1, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
         }

        mResolutionMenu = menu.addSubMenu("Resolution");
        mResolutionList = mOpenCvCameraView.getResolutionList();
        mResolutionMenuItems = new MenuItem[mResolutionList.size()];

        resolutionItr = mResolutionList.listIterator();
        idx = 0;
        while(resolutionItr.hasNext()) {
            Size element = resolutionItr.next();
            mResolutionMenuItems[idx] = mResolutionMenu.add(2, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
         }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item.getGroupId() == 1)
        {
        }
        else if (item.getGroupId() == 2)
        {
            int id = item.getItemId();
            Size resolution = mResolutionList.get(id);
            mOpenCvCameraView.setResolution(resolution);
            resolution = mOpenCvCameraView.getResolution();
            String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
            Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    private boolean initDetector() {

        mDetector = new Detector(getApplicationContext());
        Mat template = null;
        try {
            template = Utils.loadResource(getApplicationContext(), R.drawable.bayer_rect_760, 1);
            //Imgproc.resize(template,template, new org.opencv.core.Size(517,378));
        }catch(Exception e) {
            Log.e(TAG, "Mistake in load template " + e.toString());
            Toast.makeText(this, "Mistake in load template", Toast.LENGTH_SHORT).show();
            return false;
        }
        String errorMsg = "";
        Imgproc.cvtColor(template, template, Imgproc.COLOR_RGBA2BGR, 3);
        mDetector.setLogoInformation(template);

        return true;
    }


    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG, "onTouch event");

        boolean emulator = true;
        mOpenCvCameraView.setDetector(mDetector);
        if (!emulator) {
            mOpenCvCameraView.focusOnTouch(event);
        } else {
            Mat bgrMat = new Mat();
            Mat cScene = new Mat();

            try {
                bgrMat = Utils.loadResource(getApplicationContext(), R.drawable.original_scene, 1);
                //Imgproc.resize(template,template, new org.opencv.core.Size(300,300));
            } catch (Exception e) {
                Log.e(TAG, "Mistake in load fake " + e.toString());

            }
            Imgproc.cvtColor(bgrMat, cScene, Imgproc.COLOR_BGR2RGBA);
            mDetector.detect(cScene, "");
        }

        /*
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        String fileName = Environment.getExternalStorageDirectory().getPath() +
                               "/sample_picture_" + currentDateandTime + ".jpg";




        mOpenCvCameraView.detectPicture(mDetector);
        */
        /*
        Detector detector = new Detector(getApplicationContext());
        Mat template = null;
        Mat image = null;
        try {
            template = Utils.loadResource(getApplicationContext(), R.drawable.forte_template, 1);
            image = Utils.loadResource(getApplicationContext(), R.drawable.asperin_forte, 1);
        }catch(Exception e) {
            Log.e(TAG, "Mistake in load tamplate " + e.toString());
            Toast.makeText(this, "Mistake in load tamplate", Toast.LENGTH_SHORT).show();
            return false;
        }

        String errorMsg = "";
        Imgproc.cvtColor(template, template, Imgproc.COLOR_RGBA2BGR, 3);
        detector.setLogoInformation(template);
        detector.detect(currentFrame, errorMsg);
        */

        Toast.makeText(this, "touched", Toast.LENGTH_SHORT).show();

        return false;
    }
}
