package org.opencv.samples.bayer;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.features2d.DMatch;
import org.opencv.core.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.core.KeyPoint;
//import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Detector {

    private static final String TAG = "LogDetector";


    private FeatureDetector mDetector ;
	private DescriptorExtractor mExtractor;
	private DescriptorMatcher mMatcher;
	
	
	private Mat mLogo;
	private Mat mFrame;
	
	private MatOfKeyPoint mokLogo;
	private MatOfKeyPoint mokFrame;
	
	private Mat descriptorLogo;
	private Mat descriptorFrame;
	
	double DISTANCE_FAKTOR = 2.5;
	
	boolean DEBUG = true;
    boolean DEBUG_FILE = true;
	String DEBUG_PATH = "/";

	public Detector( Context context){

		this.mDetector = FeatureDetector.create(FeatureDetector.ORB);

        /*
        File outputDir = getCacheDir(); // If in an Activity (otherwise getActivity.getCacheDir();
        File outputFile = File.createTempFile("orbDetectorParams", ".YAML", outputDir);
        writeToFile(outputFile, "%YAML:1.0\nscaleFactor: 1.2\nnLevels: 8\nfirstLevel: 0 \nedgeThreshold: 31\npatchSize: 31\nWTA_K: 2\nscoreType: 1\nnFeatures: 500\n");
        orbDetector.read(outputFile.getPath());
        */

        this.mExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		this.mMatcher= DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMINGLUT);
	}

    void logMessage(String msg){
        Log.d(TAG, msg);
		if (DEBUG_FILE)
		{
			File path = getStorageDir("Bayer");
			File logFile = new File(path,"log.txt");
			if (!logFile.exists())
			{
				try
				{
					logFile.createNewFile();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try
			{
				//BufferedWriter for performance, true to set append to file flag
				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				buf.append(msg);
				buf.newLine();
				buf.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}

	void setLogoInformation(Mat logo){

        // Imgproc.resize(logo, logo, new org.opencv.core.Size( logo.size().width / 4, logo.size().height/4));


        this.mLogo = logo;
        long endTime = System.nanoTime();

        //Imgproc.GaussianBlur(logo, logo, new Size(3, 3), 2, 2);
        //Imgproc.medianBlur(logo, logo, 5);

        Imgproc.cvtColor(logo,this.mLogo, Imgproc.COLOR_RGB2GRAY);

		this.descriptorLogo =  new Mat();
		this.mokLogo = new  MatOfKeyPoint();

		this.mDetector.detect(this.mLogo, this.mokLogo);
		this.mExtractor.compute(this.mLogo, this.mokLogo, this.descriptorLogo);

        Mat circles = new Mat();
        //HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );
        //Imgproc.HoughCircles(this.mLogo, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 10);


        for (int i = 0; i < circles.cols(); i++) {
            double[] vCircle = circles.get(0, i);

            Point pt = new Point(Math.round(vCircle[0]), Math.round(vCircle[1]));
            int radius = (int)Math.round(vCircle[2]);

            Imgproc.circle(mLogo, pt, radius, new Scalar(255, 0, 0), 2);

        }

        if (DEBUG) {
	        logMessage(String.format("DescriptorLogo count %s", this.descriptorLogo.size()));
            logMessage(String.format("KeyPointLogo count %s", this.mokLogo.size()));
		}
    	

        if (DEBUG_FILE) {
    		Mat outputLogoImg = new Mat();
    		Features2d.drawKeypoints( this.mLogo, this.mokLogo, outputLogoImg);
            SaveImage(outputLogoImg,String.format("name_%s.jpg","1"));
    	}
	}

    public File getStorageDir(String folderName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), folderName);
        if (!file.mkdirs()) {
			//logMessage( "Directory not created");
        }
        return file;
    }


    public void SaveImage (Mat mat,String filename) {
        Boolean bool = false;
        try {
            Mat mIntermediateMat = new Mat();

            Imgproc.cvtColor(mat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

            File path = getStorageDir("Bayer");

            File file = new File(path, filename);
            logMessage(file.toString());

            filename = file.toString();
            bool = Imgcodecs.imwrite(filename, mIntermediateMat);
        }catch (Exception e){
            logMessage("SaveImage - Exception: " + e.getMessage());
        }

        if (bool == true)
			logMessage("SUCCESS writing image to external storage");
        else
			logMessage( "Fail writing image to external storage");
    }

	void setSceneInformation(Mat mScene){
        this.mFrame = new Mat();

		long startTime = System.nanoTime();
        Imgproc.cvtColor(mScene, this.mFrame, Imgproc.COLOR_RGBA2RGB);
        long endTime = System.nanoTime();

        //Imgproc.GaussianBlur(this.mFrame, this.mFrame, new Size(3, 3), 2, 2);

        //Imgproc.medianBlur(this.mFrame, this.mFrame, 5);
        Imgproc.cvtColor(this.mFrame, this.mFrame, Imgproc.COLOR_RGB2GRAY);

        if (DEBUG) {
            logMessage(String.format("convert rgba to rgb %d", (endTime - startTime) / 1000000));
        }


		this.mokFrame = new MatOfKeyPoint();
		this.descriptorFrame = new Mat();
		mDetector.detect(this.mFrame, this.mokFrame);
		mExtractor.compute(this.mFrame, this.mokFrame, this.descriptorFrame);
				        
		endTime = System.nanoTime();

        Mat circles = new Mat();
        //HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );
        //Imgproc.HoughCircles(this.mFrame, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 10);


        for (int i = 0; i < circles.cols(); i++) {
            double[] vCircle = circles.get(0, i);

            Point pt = new Point(Math.round(vCircle[0]), Math.round(vCircle[1]));
            int radius = (int)Math.round(vCircle[2]);

            Imgproc.circle(mLogo, pt, radius, new Scalar(255, 0, 0), 2);

        }

	    if (DEBUG) {
            logMessage(String.format("Compute Scene in ms: %d", (endTime - startTime) / 1000000));
            logMessage(String.format("DescriptorScene count %s", this.descriptorFrame.size().toString() ));
            logMessage(String.format("KeyPointScene count %s", this.mokFrame.size().toString()));

        }
        
        if (DEBUG_FILE){
			Mat outputKeypointImg = new Mat();
            Features2d.drawKeypoints(mFrame, this.mokFrame, outputKeypointImg);

            SaveImage(outputKeypointImg,"all_scene_keypoints.jpg");
	    }
	}
	
	int checkHomography(Mat  H)
    {

	  double det = H.get(0, 0)[0] * H.get( 1, 1)[0] - H.get( 1, 0)[0] * H.get( 0, 1)[0];
	  if (det < 0)
	  {	
		  if (DEBUG){
			  logMessage(String.format("checkHomography det %d",det));
		  }
		  return 1;
	  }
	  
	  double N1 = Math.sqrt(H.get(0, 0)[0] * H.get(0, 0)[0] + H.get( 1, 0)[0] * H.get( 1, 0)[0]);
	  if (N1 > 4 || N1 < 0.1)
	  {
		  if (DEBUG){
              logMessage(String.format("checkHomography N1 %d",N1));
		  }
		  return 2;
	  }
	  
	  double N2 = Math.sqrt(H.get(0, 1)[0] * H.get(0, 1)[0] + H.get(1, 1)[0] * H.get(1, 1)[0]);
	  if (N2 > 4 || N2 < 0.1)
	  {
		  if (DEBUG){
              logMessage(String.format("checkHomography N2 %d",N2));
		  }
		  return 3;
	  }
	  
	  double N3 = Math.sqrt( H.get(  2, 0)[0] *  H.get( 2, 0)[0] +  H.get(  2, 1)[0] *  H.get(  2, 1)[0]);
	  if (N3 > 0.005) //original 0.02
	  {
		  if (DEBUG){
              logMessage(String.format("checkHomography N3 %d",N3));
		  }
		  return 4;
	  }

	  return 0;

	}


    public Mat detect(byte[] bScene, String errorMsg) {
        Mat jpegData = new Mat(1, bScene.length, CvType.CV_8UC1);
        jpegData.put(0, 0, bScene);


        Mat bgrMat = Imgcodecs.imdecode(jpegData, 4);//IMREAD_ANYCOLOR = 4;
        Mat cScene =  new Mat();

        Imgproc.cvtColor(bgrMat, cScene, Imgproc.COLOR_BGR2RGBA);

        return detect(cScene,  errorMsg);

    }

	public Mat detect(Mat mScene, String errorMsg){

        Mat mOrginal = mScene.clone();
        if (DEBUG_FILE) {

            SaveImage(mOrginal,"original_scene.jpg");
        }

        //Imgproc.resize(mScene, mScene, new org.opencv.core.Size( mScene.size().width / 4, mScene.size().height/4));

        this.setSceneInformation(mScene);

        MatOfDMatch matches = new MatOfDMatch();
        mMatcher.match(descriptorLogo, descriptorFrame, matches);

        List<DMatch> matchesList = matches.toList();
        List<DMatch> goodMatches = new ArrayList<>();

        if (matchesList.size() < 4) {
            return mScene;
        }

        Double max_dist = 0.0;
        Double min_dist = Double.MAX_VALUE;

        for (int j = 0; j < matchesList.size(); j++) {
            Double dist = (double) matchesList.get(j).distance;
            if (dist < min_dist)
                min_dist = dist;
            if (dist > max_dist)
                max_dist = dist;
        }



        for (int j = 0; j < matchesList.size(); j++) {
            //  if (matchesList.distance < 50)
            if (matchesList.get(j).distance < (DISTANCE_FAKTOR * min_dist))
                goodMatches.add(matchesList.get(j));
        }

       /*
        Collections.sort(matchesList, new Comparator<DMatch>() {
            @Override
            public int compare(DMatch o1, DMatch o2) {
                if (o1.distance < o2.distance)
                    return -1;
                if (o1.distance > o2.distance)
                    return 1;
                return 0;
            }
        });


        if(matchesList.size()>50){
            goodMatches = matchesList.subList(0,50);
        }else{
            goodMatches = matchesList;
        }
        */

        if (DEBUG) {
            //logMessage(String.format("logo %d: all/good matches %d/%d - max %f min %f DISTANCE_FAKTOR %f lim %f ", i, matchesList.size(), goodMatches.size(), max_dist, min_dist ,DISTANCE_FAKTOR, DISTANCE_FAKTOR * min_dist));
            logMessage(String.format("logo: all/good matches %d/%d - max %f min %f DISTANCE_FAKTOR %f lim %f ", matchesList.size(), goodMatches.size(), max_dist, min_dist ,DISTANCE_FAKTOR, DISTANCE_FAKTOR * min_dist));
        }

        if (goodMatches.size() < 4) {
            goodMatches.clear();
        }

        List<KeyPoint> keypoints_sceneList = this.mokFrame.toList();
        List<KeyPoint> keypoints_objectList = this.mokLogo.toList();

        LinkedList<Point> objList = new LinkedList<Point>();
        LinkedList<Point> sceneList = new LinkedList<Point>();

        for (int j = 0; j < goodMatches.size(); j++) {
            objList.addLast(keypoints_objectList.get(goodMatches.get(j).queryIdx).pt);
            sceneList.addLast(keypoints_sceneList.get(goodMatches.get(j).trainIdx).pt);
        }

        MatOfPoint2f obj = new MatOfPoint2f();
        MatOfPoint2f scene = new MatOfPoint2f();

        obj.fromList(objList);
        scene.fromList(sceneList);

        if (DEBUG_FILE) {
            Mat outputImg = new Mat();
            MatOfByte drawnMatches = new MatOfByte();
            MatOfDMatch good_Matches = new MatOfDMatch();
            good_Matches.fromList(goodMatches);

            Features2d.drawMatches(mLogo, mokLogo, mFrame, mokFrame, good_Matches, outputImg, new Scalar(0, 255, 0), new Scalar(0, 0, 255), drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);

            SaveImage(outputImg, String.format("matches_scene_%s.jpg", ""));
        }


        Mat H = null;

        try {
            H = Calib3d.findHomography(obj, scene, Calib3d.RANSAC, 5);
        } catch (Exception e) {
            errorMsg = "error by findHomography\n" + e.getMessage();
        }

        Mat tmp_corners = new Mat(4, 1, CvType.CV_32FC2);
        Mat scene_corners = new Mat(4, 1, CvType.CV_32FC2);

        //get corners from object
        tmp_corners.put(0, 0, new double[]{0, 0});
        tmp_corners.put(1, 0, new double[]{mLogo.cols(), 0});
        tmp_corners.put(2, 0, new double[]{mLogo.cols(), mLogo.rows()});
        tmp_corners.put(3, 0, new double[]{0, mLogo.rows()});

        try {
            Core.perspectiveTransform(tmp_corners, scene_corners, H);
        } catch (Exception e) {
            logMessage("error by perspectiveTransform:" + e.getMessage());
        }

        Imgproc.line(mOrginal, new Point(scene_corners.get(0, 0)), new Point(scene_corners.get(1, 0)), new Scalar(0, 255, 0), 4);
        Imgproc.line(mOrginal, new Point(scene_corners.get(1, 0)), new Point(scene_corners.get(2, 0)), new Scalar(0, 255, 0), 4);
        Imgproc.line(mOrginal, new Point(scene_corners.get(2, 0)), new Point(scene_corners.get(3, 0)), new Scalar(0, 255, 0), 4);
        Imgproc.line(mOrginal, new Point(scene_corners.get(3, 0)), new Point(scene_corners.get(0, 0)), new Scalar(0, 255, 0), 4);

        if (DEBUG_FILE) {
            SaveImage(mOrginal, String.format("matches_scene_rec_%s.jpg", ""));
        }

        Mat quad = new Mat(mLogo.size(), CvType.CV_32F);
        Imgproc.warpPerspective(mOrginal, quad, H, quad.size(), Imgproc.CV_WARP_INVERSE_MAP);

        Mat bayerLogo = quad.submat(0,500,0,500).clone();

        findConture(bayerLogo);

        if (DEBUG_FILE) {
            SaveImage(bayerLogo, "bayerLogo.jpg");
            SaveImage(quad, "cutted.jpg");
        }


        if (DEBUG_FILE) {
            SaveImage(mOrginal, "matches_result.jpg");
        }

        return mOrginal;
	}

    public Mat findConture(Mat input) {

        Mat imageGray = new Mat();
        Mat cannyMat = new Mat();

        Imgproc.cvtColor(input, imageGray, Imgproc.COLOR_RGBA2GRAY);

        int edgeThresh = 15;
        Imgproc.blur(imageGray, imageGray, new Size(5, 5));
        Imgproc.Canny(imageGray, cannyMat, edgeThresh, edgeThresh * 3, 3, false);
        Imgproc.blur(cannyMat, cannyMat, new Size(3, 3));

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        List<MatOfPoint> bigContours = new ArrayList<MatOfPoint>();
        List<MatOfPoint> circleContours = new ArrayList<MatOfPoint>();
        List<MatOfPoint2f> circleContours2f = new ArrayList<MatOfPoint2f>();

        List<MatOfPoint2f> approxContours2f = new ArrayList<MatOfPoint2f>();

        double minAreaSize = 200;

        Imgproc.findContours(cannyMat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (int idx = 0; idx < contours.size(); idx++) {
            double area = Imgproc.contourArea(contours.get(idx));
            if (area > minAreaSize) {

                MatOfPoint2f tmpContours2f = new MatOfPoint2f();
                MatOfPoint2f approxContour2f = new MatOfPoint2f();
                MatOfPoint approxContour = new MatOfPoint();

                contours.get(idx).convertTo(tmpContours2f, CvType.CV_32FC2);
                Imgproc.approxPolyDP(tmpContours2f, approxContour2f, 8, true);
                approxContour2f.convertTo(approxContour, CvType.CV_32S);
                bigContours.add(approxContour);
                if (area > 75000 && area < 85000) {
                    circleContours.add(approxContour);
                    circleContours2f.add(approxContour2f);
                }

            }
        }

        Point center = new Point();
        float[] radius = new float[3];
        double totalX = 0.0, totalY = 0.0;
        for (int idx = 0; idx < circleContours2f.size(); idx++) {
            Imgproc.minEnclosingCircle(circleContours2f.get(idx), center, radius);
            break;
        }

        Point upperB = new Point(center.x + 3 ,center.y - 125);
        Point leftB = new Point(center.x - 117 ,center.y - 10);
        Point bottomR = new Point(center.x + 0,center.y + 112);
        Point rightR = new Point(center.x + 119 ,center.y - 8);
        Point middle = new Point(center.x + 0,center.y - 25);

        Imgproc.drawContours(imageGray, circleContours, -1, new Scalar(255, 0, 0), 1);
        Imgproc.line(imageGray, center, upperB, new Scalar(255, 0, 0));
        Imgproc.line(imageGray, center, bottomR, new Scalar(255, 0, 0));
        Imgproc.line(imageGray, center, rightR, new Scalar(255, 0, 0));
        Imgproc.line(imageGray, center, leftB, new Scalar(255, 0, 0));


        Mat matUpperB = getSubmat(input,upperB, 2);
        Mat matLeftB = getSubmat(input,leftB, 2);
        Mat matBottomR = getSubmat(input,bottomR, 2);
        Mat matRightR = getSubmat(input,rightR, 2);
        Mat matMiddle = getSubmat(input,middle, 2);

        Imgproc.cvtColor(imageGray, imageGray, Imgproc.COLOR_GRAY2BGR);
        SaveImage(imageGray, "Contures.png");

        compareMat(matMiddle, matUpperB, 20) ;
        compareMat(matMiddle, matLeftB, 20);
        compareMat(matMiddle, matBottomR, 20);
        compareMat(matMiddle, matRightR, 20);

        return input;
    }


    public Mat getSubmat(Mat fragmentMat, Point p, int size) {
        return fragmentMat.submat(((int)p.y)-size,((int)p.y)+size,((int)p.x)-size,((int)p.x)+size);
    }

    public List<Double> getHSVMedian(Mat fragmentMat) {
        List<Double> ret = new ArrayList<Double>();
        List<Mat> histHSV =  getHSVChannels(fragmentMat);
        Double sum = 0.0, sumCount = 0.0;
        for (int i = 0; i< histHSV.size();i++){
            sum = 0.0;
            sumCount = 0.0;
            for (int j = 0; j< histHSV.get(i).rows();j++){

                if (histHSV.get(i).get(j, 0)[0] > 0.0){
                    sum += (j+1) * histHSV.get(i).get(j, 0)[0];
                    sumCount +=  histHSV.get(i).get(j, 0)[0];
                }
            }

            ret.add(sum/sumCount);
        }

        return ret;

    }

    public List<Mat> getHSVChannels(Mat fragmentMat) {

        List<Mat> ret = new ArrayList<Mat>();
        Mat fragmentHSVMat = new Mat();
        Mat fragmentYellowMask = new Mat();

        Imgproc.cvtColor(fragmentMat, fragmentHSVMat, Imgproc.COLOR_BGR2HSV);

        Mat histFragmentHMat = new Mat();
        Mat histFragmentSMat = new Mat();
        Mat histFragmentVMat = new Mat();

        ArrayList<Mat> histImages=new ArrayList<Mat>();

        histImages.clear();
        histImages=new ArrayList<Mat>();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
                new MatOfInt(0),
                new Mat(),
                histFragmentHMat,
                new MatOfInt( 180),
                new MatOfFloat(0f,180f ),
                false);
        ret.add(histFragmentHMat);

        histImages.clear();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
                new MatOfInt(1),
                new Mat(),
                histFragmentSMat,
                new MatOfInt(256),
                new MatOfFloat(0f, 256f),
                false);
        ret.add(histFragmentSMat);

        histImages.clear();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
                new MatOfInt(2),
                new Mat(),
                histFragmentVMat,
                new MatOfInt(256),
                new MatOfFloat(0f, 256f),
                false);

        ret.add(histFragmentVMat);


        return ret;

    }




    public int compareMat(Mat firstMat, Mat secondMat, int delta) {

        List<Double> medianHSVMiddle = getHSVMedian(firstMat);
        List<Double> medianHSVLeft = getHSVMedian(secondMat);



        int cCountS = 0;

        if (medianHSVMiddle.get(1) >= medianHSVLeft.get(1) + delta)
            return 1;
        else
            return 0;

    }
}


